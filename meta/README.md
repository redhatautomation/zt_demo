# Build the execution environment for this demo

The firewalld_info module requires ansible.posix 1.3.0 or later.

To create a new execution environment(EE) which contains this collection, you can leverage the execution-environment.yml and requirements.yml files in this directory.  

The image will be based on the `registry.redhat.io/ansible-automation-platform-21/ee-minimal-rhel8` container image if no changes are made to the execution-environment.yml.

In order for the build process to succeed the EE will need access to a RHEL 8 host's repositories, as it is based upon a RHEL 8 UBI.  

From your RHEL 8 host, you simply need to execute `ansible-builder build -t <your_registry/image_name:tag>` in this directory to build the new EE.    
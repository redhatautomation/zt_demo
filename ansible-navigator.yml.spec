---
ansible-navigator:
  execution-environment:
    image: your.registry.com:5000/zt_demo:latest
    pull:
      policy: tag # up to you
    
  playbook-artifact:
    enable: true
    replay: /tmp/zt_demo.json
    save-as: /tmp/zt_demo.json
